//Node module to convert csv into json object
const csv=require('csvtojson')  


//function to set data up in rest api call
function getGladiatorsByYear(req, res) {

  //Defined const for storing csv file data.
  const csvFilePath = 'gladiators.csv'

  //using csvtojson module to load the csv file and converting it into json object.
  csv()
    //Checking the path                        
    .fromFile(csvFilePath)
    .then((jsonObj) => {
      try {
        //const to store the result of filter function by last year given from request parameters.
        const gladiatorBylastYear = jsonObj.filter((i) => i["last year"] == req.params.year)
        //check condition.
        if (gladiatorBylastYear.length == 0) {
          res.status(404).send('year not found');
        }
        //response sent by gladiatorBylastYear.
        else
          res.json(gladiatorBylastYear);
      
      }
      catch (err) {
        res.send("ServerError" + err);
      }
    });
}
//export the function so it can be used in routesPublic.
module.exports = {
  getGladiatorsByYear
};